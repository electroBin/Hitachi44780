;Demonstrate interfacing a Hitachi 44780 LCD with a PIC16F1937

	list		p=16f1937	;list directive to define processor
	#include	<p16f1937.inc>		; processor specific variable definitions
	
	errorlevel -302	;no "register not in bank 0" warnings
	errorlevel -312     ;no  "page or bank selection not needed" messages
	
	#define BANK0  (h'000')
	#define BANK1  (h'080')
	#define BANK2  (h'100')
	#define BANK3  (h'180')

	__CONFIG _CONFIG1,    _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC_XT & _FCMEN_OFF & _IESO_OFF

;Context saving variables:
CONTEXT	UDATA_SHR
w_temp		RES 	1	; variable used for context saving
status_temp	RES	1	; variable used for context saving
pclath_temp	RES	1


	
;General Variables
GENVAR	UDATA_SHR
cnt1Millis	RES	1
userMillis	RES	1
stringIndex1	RES	1
stringIndex2	RES	1


;**********************************************************************
	ORG		0x000	
	pagesel		start	; processor reset vector
	goto		start	; go to beginning of program
INT_VECTOR:
	ORG		0x004		; interrupt vector location
INTERRUPT:

    ;SUBROUTINES:	
;variable length mllisecond delay (number of millis needed is already in work register)
delayMillis
	movwf	userMillis
	clrf	cnt1Millis	;clear out millisecond counter
	banksel	TMR0
	clrf	TMR0		;clear out TMR0
waitTMR0
	banksel	TMR0
	movfw	TMR0
	xorlw	.250		;250 * 4uS = 1mS
	btfss	STATUS, Z	;1 millisecond passed?
	goto	waitTMR0	;no so keep looping
	incf	cnt1Millis, f	;yes so increment millisecond counter
	
	movfw	userMillis	;move user desired # of mS into counter variable
	xorwf	cnt1Millis, w	
	btfss	STATUS, Z	;test whether current mS count is equal to desired
				;number of milliseconds
	goto	waitTMR0	;no so keep incrementing mS counter
	movlw	b'11000001'	;T0CS=0 (timer mode), PSA=0 (prescaler assigned to timer), PS=001 (prescale=4)
	banksel	OPTION_REG		;increment TMR0 every 4 us
	movwf	OPTION_REG	;TMR0 overflows every 1 ms
	retlw	0
	
;Set command mode
RS0
	banksel	PORTB
	bcf		PORTB, 2	
	movlw	.1
	call	delayMillis
	retlw	0
;Set character mode
RS1
	banksel	PORTB
	bsf		PORTB, 2	
	movlw	.1
	call	delayMillis
	retlw	0
;Pulse E (enable)
ePulse
	banksel	PORTB
	bsf		PORTB, 1		;take E line high
	nop	
	nop
	nop					;hold for 3 clock cycles
	bcf		PORTB, 1		;take E line low
	retlw	0
	
;Send a command to LCD (command is already in work register)
sendCommand
	banksel	PORTD
	movwf	PORTD	    ;send command to PORTD
	call	RS0	        ;Enter command mode
	call	ePulse	    ;pulse E line
	movlw	.2
	call	delayMillis
	retlw	0
	
;Send character data to LCD (data is already in work register)
sendData
	banksel	PORTD
	movwf	PORTD	    ;send character to PORTD
	call	RS1	        ;Enter character mode
	call	ePulse	    ;pulse E line
	movlw	.2
	call	delayMillis
	retlw	0
	

	
;***************Initialize LCD**************************************************
LCDInit
;50ms startup delay
	movlw	.50
	call	delayMillis

	movlw	b'00111000'	;user-defined "function set" command
	call	sendCommand
    
	;confirm entry mode set
	movlw	b'00000110'	;increment cursor, display shift off
	call	sendCommand
	
	;Display on, cursor on, blink on
	movlw	b'00001111'
	call	sendCommand
	
	;LCD is now active and will display any data printed to DDRAM
	retlw	0
	
getString1
	movlw	    LOW string1
	addwf	    stringIndex1, w
	btfsc	    STATUS, C
	incf	    PCLATH, f
	movwf	    PCL
string1	dt	    "Hitachi", 0
	
getString2
	movlw	    LOW string2
	addwf	    stringIndex2, w
	btfsc	    STATUS, C
	incf	    PCLATH, f
	movwf	    PCL
string2	dt	    "44780", 0

start:
	banksel BANK0
	movlw   b'00000000'
	movwf   PORTA
	movlw   b'00000000'
	movwf   PORTB
	movlw   b'00000000'
	movwf   PORTC
	movlw   b'00000000'
	movwf   PORTD
	movlw   b'00000000'
	movwf   PORTE

	banksel BANK1
    ;Set PORTS to output
	movlw   b'00000000'		    
	movwf   (TRISA ^ BANK1)
	movlw   b'00000000'
	movwf   (TRISB ^ BANK1)
	movlw   b'00000000'		    
	movwf   (TRISC ^ BANK1)
	movlw   b'00000000'
	movwf   (TRISD ^ BANK1)
	movlw   b'00000000'
	movwf   (TRISE ^ BANK1)
    
	banksel ANSELB
	clrf    ANSELB                    
	clrf    ANSELD
	clrf    ANSELE
  
	movlw   b'01101000'
	banksel OSCCON
	movwf   OSCCON
    
;Configure Timer0:
	movlw	b'11000001'	;T0CS=0 (timer mode), PSA=0 (prescaler assigned to timer), PS=001 (prescale=4)
	banksel	BANK1		;increment TMR0 every 4 us
	movwf	OPTION_REG	;TMR0 overflows every 1 ms
    
	clrf    cnt1Millis	
	clrf    userMillis	
	clrf    stringIndex1	
	clrf    stringIndex2
    
	pagesel	LCDInit
	call	LCDInit
	clrf	stringIndex1
	clrf	stringIndex2
    ;Clear display and reset cursor
	movlw	b'00000001'
	call	sendCommand
	movlw	.20
	call	delayMillis
mainLoop
    
printL1
    ;get next character from string:
	call	getString1	;display first name
	;finish if null
	iorlw	0
	btfsc	STATUS, Z	;end of string?
	goto	doneLine1	;if end of string quit looping
	;Write character to display:
	call	sendData	;write char
	movlw	.1
	call	delayMillis	;delay
	incf	stringIndex1	;increment string index to point to next char
	goto	printL1
doneLine1
	;Set display address to beginning of second line
	movlw	0xC0
	call	sendCommand
printL2
    ;get next character from string:
	call	getString2	;display first name
	;finish if null
	iorlw	0
	btfsc	STATUS, Z	;end of string?
	goto	doneLine2	;if end of string quit looping
	;Write character to display:
	call	sendData	;write char
	movlw	.1
	call	delayMillis	;delay
	incf	stringIndex2	;increment string index to point to next char
	goto	printL2
doneLine2
	;Set display address to beginning of 3rd line
	movlw	0x94
	call	sendCommand
	;Print single characters
	movlw	b'01010000' ;P
	call	sendData
	movlw	b'01001001' ;I
	call	sendData
	movlw	b'01000011' ;C
	call	sendData

	;Set display address to beginning of 4th line
	movlw	0xD4
	call	sendCommand
	;Print single characters
	movlw	b'00110001' ;1
	call	sendData
	movlw	b'00110110' ;6
	call	sendData
	movlw	b'01000110' ;F
	call	sendData
	movlw	b'00110001' ;1
	call	sendData
	movlw	b'00111001' ;9
	call	sendData
	movlw	b'00110011' ;3
	call	sendData
	movlw	b'00110111' ;7
	call	sendData
doneLine4
	goto	doneLine4
   
	END                       









